var n1 = document.getElementById('n1');
var n2 = document.getElementById('n2');
var n3 = document.getElementById('n3');
var n4 = document.getElementById('n4');
var n5 = document.getElementById('n5');
var n6 = document.getElementById('n6');
function add() {
    let num1 = parseFloat(n1.value);
    let num2 = parseFloat(n2.value);
    if (isNaN(num1) || isNaN(num2)) {
        alert("Please enter valid number");
        return;
    }
    n3.value = (num1 + num2).toString();
}
function sub() {
    let num1 = parseFloat(n1.value);
    let num2 = parseFloat(n2.value);
    if (isNaN(num1) || isNaN(num2)) {
        alert("Please enter valid number");
        return;
    }
    n4.value = (num1 - num2).toString();
}
function mul() {
    let num1 = parseFloat(n1.value);
    let num2 = parseFloat(n2.value);
    if (isNaN(num1) || isNaN(num2)) {
        alert("Please enter valid number");
        return;
    }
    n5.value = (num1 * num2).toString();
}
function div() {
    let num1 = parseFloat(n1.value);
    let num2 = parseFloat(n2.value);
    if (isNaN(num1) || isNaN(num2)) {
        alert("Please enter valid number");
        return;
    }
    n6.value = (num1 / num2).toString();
}
//# sourceMappingURL=app.js.map