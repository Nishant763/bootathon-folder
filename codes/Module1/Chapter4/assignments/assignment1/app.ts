var n1:HTMLInputElement = <HTMLInputElement> document.getElementById('n1');
var n2:HTMLInputElement = <HTMLInputElement> document.getElementById('n2');
var n3:HTMLInputElement = <HTMLInputElement> document.getElementById('n3');
var n4:HTMLInputElement = <HTMLInputElement> document.getElementById('n4');
var n5:HTMLInputElement = <HTMLInputElement> document.getElementById('n5');
var n6:HTMLInputElement = <HTMLInputElement> document.getElementById('n6');

function add(){
    let num1:number = parseFloat(n1.value);
    let num2:number = parseFloat(n2.value);  
    if(isNaN(num1)||isNaN(num2)){
        alert("Please enter valid number");
        return;
    }
    n3.value = (num1+num2).toString();
}

function sub(){
    let num1:number = parseFloat(n1.value);
    let num2:number = parseFloat(n2.value);  
    if(isNaN(num1)||isNaN(num2)){
        alert("Please enter valid number");
        return;
    }
    n4.value = (num1-num2).toString();
}

function mul(){
    let num1:number = parseFloat(n1.value);
    let num2:number = parseFloat(n2.value);  
    if(isNaN(num1)||isNaN(num2)){
        alert("Please enter valid number");
        return;
    }
    n5.value = (num1*num2).toString();
}

function div(){
    let num1:number = parseFloat(n1.value);
    let num2:number = parseFloat(n2.value);  
    if(isNaN(num1)||isNaN(num2)){
        alert("Please enter valid number");
        return;
    }
    n6.value = (num1/num2).toString();
}