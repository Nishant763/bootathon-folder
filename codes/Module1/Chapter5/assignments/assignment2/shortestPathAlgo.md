# Shortest Paths In Graphs

## Bellman-Ford

Lets look at a simple method, by extending the approach used in BFS. We define ds(v) as the minimum cost of a path from s to v. Initially ds(v) = ∞ for all vertices v except s, and ds(s) = 0. Recall that in BFS, we say a node is finished if it is in the queue. If we directly enqueue all the neighbors v of a vertex u in to the queue and mark them as finished, it may not work for this weighted version. There can be another path from u to v with more number of edges but with smaller cost. So, we may have to change ds(v) while v is still in queue. If you consider the same argument using paths with more edges but with smaller cost, even this may not work. We may need to update ds(v) even when v is not in queue and can do so as long as there are changes to some ds(v). This suggests that we need not have a queue and as there are only a finite number of edges in any shortest path, the updations will stop eventually. This is the Bellman-Ford single source shortest path algorithm and is descibed below.<br>
<b>*Algorithm Bellman-Ford(G,S)*</b><br>
*for all vertices v do*<br>
*ds(v) = ∞; from(v) = NIL;*<br>
*end-for*<br>
*for n-1 iterations do*<br>
*for each edge (v,w) do*<br>
*if ds(w) > ds(v) + W(v,w) then<br>
ds(w) = ds(v) + W(v,w); from(w) = v;<br>
end-if <br>end-for<br>
end-for<br>
<b>End-Bellman-Ford</b>*


>The algorithm requires <i>O(mn)</i> time, where n is the number of vertices and m is the number of edges. For each of the n-1 iterations, we consider each edge once.
